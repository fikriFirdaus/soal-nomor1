
package ark_wa3_si4106_1202171139_fiqri;

import java.util.Scanner;//fungsi Scanner
import java.util.Random;//fungsi random
public class ARK_WA3_SI4106_1202171139_FIQRI {

    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);//fungsi untuk input jawaban kita
        Random masuk = new Random();//fungsi untuk mengacak angka
        int tebak;// variabel 
        int acak ;
                
        System.out.println("===============================");
        System.out.println("----- PROGRAM TEBAK ANGKA -----");
        System.out.println("===============================");
        acak = masuk.nextInt(50);//gunanya menentukan angka acak dari 1-50
        do {
            System.out.print("Masukkan tebakan:");
            tebak = input.nextInt();// untuk menginput tebakan kita
            
            if(tebak > acak){// jika angka yg kita tebak ternyata lbih besar dri angka acak yg telah ditentukan maka akan tampil tulisan dibawah
                System.out.println("Tebakkan anda terlalu besar");
            } else if(tebak < acak){ //jika angka yg kita tebak ternyata lbih besar dri angka acak yg telah ditentukan maka akan tampil tulisan dibawah
                System.out.println("Tebakkan anda terlalu kecil");
            }
        } while (tebak != acak);// perulangan tidak akan berhenti jika angka yg kita tebak tidak sama dengan angka acak yg telah ditentukan 
        System.out.println("");
        System.out.println("Selamat, Anda berhasil menyelesaikan permainan.");
//        
    } 
    
}
